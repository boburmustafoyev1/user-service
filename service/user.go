package service

import (
	"context"
	"user_service/genproto/product"
	pb "user_service/genproto/user"
	l "user_service/pkg/logger"
	g "user_service/service/grpc_client"
	"user_service/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	clienet g.GrpcClient
	storage storage.IStorage
	logger  l.Logger
}

// NewProductService ...

func NewUserService(db *sqlx.DB, log l.Logger, client g.GrpcClient) *UserService {
	return &UserService{
		clienet: client,
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (s *UserService) CreateUser(ctx context.Context, req *pb.User) (*pb.User, error) {
	user, err := s.storage.User().CreateUser(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("Error insert user", err))
		return &pb.User{}, status.Error(codes.Internal, "something went wrong,please check product infto")
	}
	return user, nil
}

func (s *UserService) GetUserById(ctx context.Context, req *pb.GetId) (*pb.User, error) {
	user, err := s.storage.User().GetUserById(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("Error insert user", err))
		return &pb.User{}, status.Error(codes.Internal, "something went wrong,please check product infto")
	}
	return user, nil
}

func (s *UserService) UpdateUser(ctx context.Context, req *pb.User) (*pb.User, error) {
	user, err := s.storage.User().UpdateUser(req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Update", err))
		return &pb.User{}, status.Error(codes.InvalidArgument, "Please recheck user info")
	}
	return user, nil
}

func (s *UserService) DeleteUser(ctx context.Context, req *pb.GetId) (*pb.Empty, error) {
	user, err := s.storage.User().DeleteUser(req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Delete", err))
		return &pb.Empty{}, status.Error(codes.InvalidArgument, "Please recheck user info")
	}
	return user, nil
}

func (s *UserService) BuyProduct(ctx context.Context, req *pb.BuyProductRequest) (*pb.BuyProductResponse, error) {
	if err := s.storage.User().BuyProduct(req); err != nil {
		s.logger.Error("Error while buying", l.Any("create", err))
		return &pb.BuyProductResponse{}, status.Error(codes.InvalidArgument, "Please recheck buy product info")
	}
	var err error
	response := &pb.BuyProductResponse{}
	response.User, err = s.storage.User().GetUserById(&pb.GetId{Id: req.UserId})
	if err != nil {
		s.logger.Error("Error while getting user info", l.Any("create", err))
		return &pb.BuyProductResponse{}, status.Error(codes.InvalidArgument, "Please recheck buy product info")
	}
	product, err := s.clienet.Product().GetProductInfoByid(ctx, &product.Ids{Id: []int64{req.ProductId}})
	if err != nil {
		s.logger.Error("Error while getting user info", l.Any("create", err))
		return &pb.BuyProductResponse{}, status.Error(codes.InvalidArgument, "Please recheck buy product info")
	}
	for _, p := range product.Products {
		tempProduct := &pb.Product{}
		tempProduct.Amount = p.Amount
		tempProduct.CategoryId = p.CategoryId
		tempProduct.Name = p.Name
		tempProduct.Model = p.Model
		tempProduct.Id = p.Id
		tempProduct.Price = p.Price
		tempProduct.TypeId = p.TypeId
		response.Products = append(response.Products, tempProduct)
	}

	return response, nil

}
