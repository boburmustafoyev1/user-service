package grpcclient

import (
	"fmt"
	"user_service/config"
	"user_service/genproto/product"
	"user_service/pkg/logger"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// GrpcClient ...
type GrpcClient struct {
	cfg            config.Config
	logger         logger.Logger
	productService product.ProductServiceClient
}

func New(cnf config.Config, l logger.Logger) (*GrpcClient, error) {
	productConnection, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cnf.ProductServiceHost, cnf.ProductServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	productS := product.NewProductServiceClient(productConnection)

	return &GrpcClient{
		logger:         l,
		cfg:            cnf,
		productService: productS,
	}, nil
}

func (c *GrpcClient) Product() product.ProductServiceClient {
	return c.productService
}