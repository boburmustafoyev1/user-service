alter table users add column balance int default 100;

CREATE TABLE IF NOT EXISTS user_products (
    owner_id int not null references users(id),
    product_id int not null,
    amount int not null
);